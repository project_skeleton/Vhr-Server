package org.javaboy.vhr.controller.security;
import org.javaboy.vhr.model.*;
import org.javaboy.vhr.service.*;
import org.javaboy.vhr.utils.POIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @version: V1.0
 * @author: qxw
 * @className: SecurityController
 * @packageName: org.javabody.vhr.controller.security
 * @description:
 * @data: 2020-03-11 17:05
 **/
@RestController
@RequestMapping("/security/basic")
public class SecurityController {
    @Autowired
    SecurityPolicyService securityPolicyService;

    @RequestMapping(value = "/basicdata", method = RequestMethod.GET)
    public Map<String, Object> getAllNations() {
        Map<String, Object> map = new HashMap<>();
        return map;
    }

    @RequestMapping(value = "/security/{ids}", method = RequestMethod.DELETE)
    public RespBean deleteSecurityById(@PathVariable String ids) {
        if (securityPolicyService.deleteStaAccounById(ids)) {
            return RespBean.ok("删除成功!");
        }
        return RespBean.error("删除失败!");
    }

    /**
     * 根据页码和大小分页查询
     * @param page 当前页，默认为1
     * @param size 当前每页显示行数，默认为10
     * @return 页信息的实体
     */
    @RequestMapping(value = "/security", method = RequestMethod.GET)
    public RespPageBean getStaAccounByPage(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size,
            @RequestParam(defaultValue = "") String keywords,
            @RequestParam(defaultValue = "") String privateIpAddress,
            @RequestParam(defaultValue = "") String destinationIPAddress,
            @RequestParam(defaultValue = "") String protocolType,
            @RequestParam(defaultValue = "") String destinationPort,
            @RequestParam(defaultValue = "") String visitBusinessDescription,
            @RequestParam(defaultValue = "") String durationVisit,
            @RequestParam(defaultValue = "") String applicationType,
            @RequestParam(defaultValue = "") String Remarks,
            @RequestParam(defaultValue = "") String businessType) {
        return securityPolicyService.getSecurityByPage(page, size,keywords,privateIpAddress, destinationIPAddress, protocolType,destinationPort, visitBusinessDescription, durationVisit,applicationType, Remarks,businessType);

    }

    @RequestMapping(value = "/exportEmp", method = RequestMethod.GET)
    public ResponseEntity<byte[]> exportSecurity() {
        //return PoiUtils.exportStaAccountExcel(staAccountService.getStaAccoun());
        return null;
    }


    @RequestMapping(value = "/importEmp", method = RequestMethod.POST)
    public RespBean importSecurity(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            String error = "上传文件格式不正确";
            return RespBean.ok(error);
        }
        List<SecurityPolicy> emps = POIUtils.getExcelDate(file);
        List<String> repeatCourseNums = new ArrayList<String>();
        repeatCourseNums=securityPolicyService.addCourseBaseInfoBatch(emps);
        //4.根据返回结果判断重复的数据与条数。
        int allTotal = emps.size();
        String reMsg="导入失败";
        System.out.println("##########################################"+allTotal);
        // 4.1如果重复的集合为空则证明全部上传成功
        if(repeatCourseNums == null || repeatCourseNums.size()==0){
            return RespBean.ok(allTotal+"条安全策略申请全部上传成功");
        }else {//4.2如果有重复提示哪些重复了
            int repeatSize = repeatCourseNums.size();
            reMsg="总共"+allTotal+"条数据，成功上传"+(allTotal - repeatSize)+"条,重复了"+repeatSize+"条。"+"重复的安全策略申请编号为"+repeatCourseNums.toString();
        }
        //导入失败
        return RespBean.error(reMsg);
    }
}