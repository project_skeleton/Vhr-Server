package org.javaboy.vhr.mapper;

import org.apache.ibatis.annotations.Param;
import org.javaboy.vhr.model.SecurityPolicy;


import java.util.List;

public interface SecurityPolicyMapper {
    /**
     * mybatis中传入参数方式一
     */
    List<SecurityPolicy> getSecurityPolicyByParam(SecurityPolicy securityPolicy);

    /**
     * mybatis中传入参数方式二
     * @Param 方式的话，一定要与配置文件中的
     *  <if test="unitname!=null and unitname!=''">的参数对应，否侧XML不识别
     *  即有几个参数，需要有几个if方便加以判断
     */
    List<SecurityPolicy> getSecurityPolicyByPage(@Param("start") Integer start, @Param("size") Integer size, @Param("keywords") String keywords, @Param("privateIPAddress") String privateIPAddress, @Param("destinationIPAddress") String destinationIPAddress, @Param("protocolType") String protocolType, @Param("destinationPort") String destinationPort, @Param("visitBusinessDescription") String visitBusinessDescription, @Param("durationVisit") String durationVisit, @Param("applicationType") String applicationType, @Param("remarks") String remarks, @Param("businessType") String businessType);


    /**
     * 同理方式二：
     */
    Long getCountByKeywords(@Param("keywords") String keywords, @Param("privateIPAddress") String privateIPAddress, @Param("destinationIPAddress") String destinationIPAddress, @Param("protocolType") String protocolType, @Param("destinationPort") String destinationPort, @Param("visitBusinessDescription") String visitBusinessDescription, @Param("durationVisit") String durationVisit, @Param("applicationType") String applicationType, @Param("remarks") String remarks, @Param("businessType") String businessType);

    /**
     * 同理方式一：
     */
    Long getCountByParam(SecurityPolicy securityPolicy);

    /**
     * 删除
     * @param ids
     * @return
     */
    int deleteStaAccounById(@Param("ids") String[] ids);


    int addSecurityPolicy(@Param("securityPolicys") List<SecurityPolicy> securityPolicys);
}
