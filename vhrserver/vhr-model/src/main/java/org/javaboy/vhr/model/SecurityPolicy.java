package org.javaboy.vhr.model;

import java.util.Date;

/**
 * 安全策略基础信息表
 */
public class SecurityPolicy {
    public Long getSpId() {
        return spId;
    }

    public void setSpId(Long spId) {
        this.spId = spId;
    }

    private Long spId;
    private String privateIPAddress;//私网IP地址
    private String destinationIPAddress;//目的地址
    private String protocolType;//协议类型
    private String destinationPort;//目的端口
    private String visitBusinessDescription;//访问业务说明
    private String durationVisit;//访问权限
    private String applicationType;//申请类型
    private String remarks;//备注
    private Date createdate;//创建时间

    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao;
    }

    private String bianhao;

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    private String businessType;//业务类型
    private Integer page;
    private Integer size;



    public String getPrivateIPAddress() {
        return privateIPAddress;
    }

    public void setPrivateIPAddress(String privateIPAddress) {
        this.privateIPAddress = privateIPAddress;
    }

    public String getDestinationIPAddress() {
        return destinationIPAddress;
    }

    public void setDestinationIPAddress(String destinationIPAddress) {
        this.destinationIPAddress = destinationIPAddress;
    }

    public String getProtocolType() {
        return protocolType;
    }

    public void setProtocolType(String protocolType) {
        this.protocolType = protocolType;
    }

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getVisitBusinessDescription() {
        return visitBusinessDescription;
    }

    public void setVisitBusinessDescription(String visitBusinessDescription) {
        this.visitBusinessDescription = visitBusinessDescription;
    }

    public String getDurationVisit() {
        return durationVisit;
    }

    public void setDurationVisit(String durationVisit) {
        this.durationVisit = durationVisit;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }
}