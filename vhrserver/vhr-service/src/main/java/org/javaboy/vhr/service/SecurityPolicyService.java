package org.javaboy.vhr.service;

import org.javaboy.vhr.mapper.SecurityPolicyMapper;
import org.javaboy.vhr.model.RespPageBean;
import org.javaboy.vhr.model.SecurityPolicy;
import org.javaboy.vhr.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @version: V1.0
 * @author: qxw
 * @className: SecurityPolicyService
 * @packageName: org.javaboy.vhr.service
 * @description: @Transactional 可以作用于接口、接口方法、类以及类方法上。当作用于类上时，该类的所有 public 方法将都具有该类型的事务属性，同时，我们也可以在方法级别使用该标注来覆盖类级别的定义。
 * @data: 2020-03-11 17:09
 **/
@Service
@Transactional
public class SecurityPolicyService {

    @Autowired
    SecurityPolicyMapper securityPolicyMapper;


    public RespPageBean getSecurityByPage(Integer page, Integer size, String keywords, String private_Ip_Address, String Destination_IP_Address, String Protocol_type, String Destination_port, String Visit_Business_Description, String Duration_visit, String Application_type, String Remarks, String businessType) {
        // 默认从0开始
        if (page != null && size != null) {
            page = (page - 1) * size;
        }
        List<SecurityPolicy> data = securityPolicyMapper.getSecurityPolicyByPage(page, size, keywords, private_Ip_Address,Destination_IP_Address, Protocol_type,Destination_port, Visit_Business_Description, Duration_visit, Application_type, Remarks,businessType);;
        Long total = getCountByKeywords(keywords,private_Ip_Address, Destination_IP_Address, Protocol_type, Destination_port, Visit_Business_Description, Duration_visit, Application_type, Remarks,businessType);
        RespPageBean bean = new RespPageBean();
        bean.setData(data);
        bean.setTotal(total);

        System.out.println("@Param注解在mybatis中的使用以及传入参数时，获得的记录当前页条数=========="+data.size());
        System.out.println("@Param注解在mybatis中的使用以及传入参数时，获得的记录列表总条数=========="+total);

        return bean;
    }



    /**
     * 实体对象形式传递参数
     * @param staAccount
     * @return
     */
    public List<SecurityPolicy> getStaRmsfeeByParam(SecurityPolicy staAccount){
        return securityPolicyMapper.getSecurityPolicyByParam(staAccount);
    }


    /**
     * 统计方式--------------参数多形参
     * @param keywords
     * @return
     */
    public Long getCountByKeywords(String keywords,String private_Ip_Address, String Destination_IP_Address, String Protocol_type, String Destination_port, String Visit_Business_Description, String Duration_visit, String Application_type, String Remarks,String businessType) {
        return securityPolicyMapper.getCountByKeywords(keywords,private_Ip_Address,Destination_IP_Address, Protocol_type,Destination_port, Visit_Business_Description, Duration_visit, Application_type, Remarks,businessType);
    }

    /**
     * 通过实体对象形式-获取
     * @param staAccount
     * @return
     */
    public Long getCountByParam(SecurityPolicy staAccount) {
        return securityPolicyMapper.getCountByParam(staAccount);
    }


    /***
     * 删除
     */
    public boolean deleteStaAccounById(String ids) {
        String[] split = ids.split(",");
        return securityPolicyMapper.deleteStaAccounById(split) == split.length;
    }

    /**
     * 导入解析EXCEL
     * @return
     */
    public int addSecurityPolicy(List<SecurityPolicy> securityPolicy){
        int abc=securityPolicyMapper.addSecurityPolicy(securityPolicy);
        System.out.println("*************************************************============="+abc);
        return abc;
    }




    /**
     * @Author qixiaowei
     * @Description //TODO
     *
     * 主要就是遍历集合，获取课程编号判断数据库中是否已经存在相同编号的数据，
     * 如果已经存在则此条数据不保存数据库并将编号加到重复的list集合。
     *
     *
     *
     * @Date 20:24 org.sang.service
     * @Param [courseBaseInfos]
     * @return java.util.List<java.lang.String>
     **/
    public List<String> addCourseBaseInfoBatch(List<SecurityPolicy> courseBaseInfos)  {
        //1.遍历集合进行添加。
        //1.1如果已经存在相同的课程编号，将该课程的编号加到返回的集合中，用于提示哪些编号重复了
        List<SecurityPolicy> repeatCourseNums = new ArrayList<SecurityPolicy>();
        List<String> rusultNums = new ArrayList<String>();
        for(SecurityPolicy securityPolicy :courseBaseInfos){
            //如果私网IP地址、目的IP地址、协议类型、目的端口、访问业务说明、访问期限、申请类型 为空结束本次循环开始下一次
            if(StringUtil.isBlank(securityPolicy.getPrivateIPAddress())
                    && StringUtil.isBlank(securityPolicy.getDestinationIPAddress())
                    && StringUtil.isBlank(securityPolicy.getProtocolType())
                    &&  StringUtil.isBlank(securityPolicy.getDestinationPort())){
                continue;
            }
            //根据数据库是否已经存在相同的 私网IP地址、目的IP地址、协议类型、目的端口、访问业务说明、访问期限、申请类型

            Long result = this.getCountByParam(securityPolicy);
            if(result >= 1){//如果存在就不添加并保存到重复的元素集合
                rusultNums.add(securityPolicy.getBianhao());
                System.out.println("*********************重复的编号：********************"+securityPolicy.getBianhao());
            }else {//不存在就可以添加
                repeatCourseNums.add(securityPolicy);
            }
        }
        //采取的是批量
        if(repeatCourseNums!=null && repeatCourseNums.size()>0){
            this.addSecurityPolicy(repeatCourseNums);
        }
        return rusultNums;
    }
}