package org.javaboy.vhr.utils;

import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import org.javaboy.vhr.model.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.xmlbeans.impl.piccolo.xml.Piccolo.STRING;

/**
 * @作者 江南一点雨
 * @公众号 江南一点雨
 * @微信号 a_java_boy
 * @GitHub https://github.com/lenve
 * @博客 http://wangsong.blog.csdn.net
 * @网站 http://www.javaboy.org
 * @时间 2019-11-11 23:25
 */
public class POIUtils {

    public static ResponseEntity<byte[]> employee2Excel(List<Employee> list) {
        //1. 创建一个 Excel 文档
        HSSFWorkbook workbook = new HSSFWorkbook();
        //2. 创建文档摘要
        workbook.createInformationProperties();
        //3. 获取并配置文档信息
        DocumentSummaryInformation docInfo = workbook.getDocumentSummaryInformation();
        //文档类别
        docInfo.setCategory("员工信息");
        //文档管理员
        docInfo.setManager("javaboy");
        //设置公司信息
        docInfo.setCompany("www.javaboy.org");
        //4. 获取文档摘要信息
        SummaryInformation summInfo = workbook.getSummaryInformation();
        //文档标题
        summInfo.setTitle("员工信息表");
        //文档作者
        summInfo.setAuthor("javaboy");
        // 文档备注
        summInfo.setComments("本文档由 javaboy 提供");
        //5. 创建样式
        //创建标题行的样式
        HSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.YELLOW.index);
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        HSSFCellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
        HSSFSheet sheet = workbook.createSheet("员工信息表");
        //设置列的宽度
        sheet.setColumnWidth(0, 5 * 256);
        sheet.setColumnWidth(1, 12 * 256);
        sheet.setColumnWidth(2, 10 * 256);
        sheet.setColumnWidth(3, 5 * 256);
        sheet.setColumnWidth(4, 12 * 256);
        sheet.setColumnWidth(5, 20 * 256);
        sheet.setColumnWidth(6, 10 * 256);
        sheet.setColumnWidth(7, 10 * 256);
        sheet.setColumnWidth(8, 16 * 256);
        sheet.setColumnWidth(9, 12 * 256);
        sheet.setColumnWidth(10, 15 * 256);
        sheet.setColumnWidth(11, 20 * 256);
        sheet.setColumnWidth(12, 16 * 256);
        sheet.setColumnWidth(13, 14 * 256);
        sheet.setColumnWidth(14, 14 * 256);
        sheet.setColumnWidth(15, 12 * 256);
        sheet.setColumnWidth(16, 8 * 256);
        sheet.setColumnWidth(17, 20 * 256);
        sheet.setColumnWidth(18, 20 * 256);
        sheet.setColumnWidth(19, 15 * 256);
        sheet.setColumnWidth(20, 8 * 256);
        sheet.setColumnWidth(21, 25 * 256);
        sheet.setColumnWidth(22, 14 * 256);
        sheet.setColumnWidth(23, 15 * 256);
        sheet.setColumnWidth(24, 15 * 256);
        //6. 创建标题行
        HSSFRow r0 = sheet.createRow(0);
        HSSFCell c0 = r0.createCell(0);
        c0.setCellValue("编号");
        c0.setCellStyle(headerStyle);
        HSSFCell c1 = r0.createCell(1);
        c1.setCellStyle(headerStyle);
        c1.setCellValue("姓名");
        HSSFCell c2 = r0.createCell(2);
        c2.setCellStyle(headerStyle);
        c2.setCellValue("工号");
        HSSFCell c3 = r0.createCell(3);
        c3.setCellStyle(headerStyle);
        c3.setCellValue("性别");
        HSSFCell c4 = r0.createCell(4);
        c4.setCellStyle(headerStyle);
        c4.setCellValue("出生日期");
        HSSFCell c5 = r0.createCell(5);
        c5.setCellStyle(headerStyle);
        c5.setCellValue("身份证号码");
        HSSFCell c6 = r0.createCell(6);
        c6.setCellStyle(headerStyle);
        c6.setCellValue("婚姻状况");
        HSSFCell c7 = r0.createCell(7);
        c7.setCellStyle(headerStyle);
        c7.setCellValue("民族");
        HSSFCell c8 = r0.createCell(8);
        c8.setCellStyle(headerStyle);
        c8.setCellValue("籍贯");
        HSSFCell c9 = r0.createCell(9);
        c9.setCellStyle(headerStyle);
        c9.setCellValue("政治面貌");
        HSSFCell c10 = r0.createCell(10);
        c10.setCellStyle(headerStyle);
        c10.setCellValue("电话号码");
        HSSFCell c11 = r0.createCell(11);
        c11.setCellStyle(headerStyle);
        c11.setCellValue("联系地址");
        HSSFCell c12 = r0.createCell(12);
        c12.setCellStyle(headerStyle);
        c12.setCellValue("所属部门");
        HSSFCell c13 = r0.createCell(13);
        c13.setCellStyle(headerStyle);
        c13.setCellValue("职称");
        HSSFCell c14 = r0.createCell(14);
        c14.setCellStyle(headerStyle);
        c14.setCellValue("职位");
        HSSFCell c15 = r0.createCell(15);
        c15.setCellStyle(headerStyle);
        c15.setCellValue("聘用形式");
        HSSFCell c16 = r0.createCell(16);
        c16.setCellStyle(headerStyle);
        c16.setCellValue("最高学历");
        HSSFCell c17 = r0.createCell(17);
        c17.setCellStyle(headerStyle);
        c17.setCellValue("专业");
        HSSFCell c18 = r0.createCell(18);
        c18.setCellStyle(headerStyle);
        c18.setCellValue("毕业院校");
        HSSFCell c19 = r0.createCell(19);
        c19.setCellStyle(headerStyle);
        c19.setCellValue("入职日期");
        HSSFCell c20 = r0.createCell(20);
        c20.setCellStyle(headerStyle);
        c20.setCellValue("在职状态");
        HSSFCell c21 = r0.createCell(21);
        c21.setCellStyle(headerStyle);
        c21.setCellValue("邮箱");
        HSSFCell c22 = r0.createCell(22);
        c22.setCellStyle(headerStyle);
        c22.setCellValue("合同期限(年)");
        HSSFCell c23 = r0.createCell(23);
        c23.setCellStyle(headerStyle);
        c23.setCellValue("合同起始日期");
        HSSFCell c24 = r0.createCell(24);
        c24.setCellStyle(headerStyle);
        c24.setCellValue("合同终止日期");
        for (int i = 0; i < list.size(); i++) {
            Employee emp = list.get(i);
            HSSFRow row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(emp.getId());
            row.createCell(1).setCellValue(emp.getName());
            row.createCell(2).setCellValue(emp.getWorkID());
            row.createCell(3).setCellValue(emp.getGender());
            HSSFCell cell4 = row.createCell(4);
            cell4.setCellStyle(dateCellStyle);
            cell4.setCellValue(emp.getBirthday());
            row.createCell(5).setCellValue(emp.getIdCard());
            row.createCell(6).setCellValue(emp.getWedlock());
            row.createCell(7).setCellValue(emp.getNation().getName());
            row.createCell(8).setCellValue(emp.getNativePlace());
            row.createCell(9).setCellValue(emp.getPoliticsstatus().getName());
            row.createCell(10).setCellValue(emp.getPhone());
            row.createCell(11).setCellValue(emp.getAddress());
            row.createCell(12).setCellValue(emp.getDepartment().getName());
            row.createCell(13).setCellValue(emp.getJobLevel().getName());
            row.createCell(14).setCellValue(emp.getPosition().getName());
            row.createCell(15).setCellValue(emp.getEngageForm());
            row.createCell(16).setCellValue(emp.getTiptopDegree());
            row.createCell(17).setCellValue(emp.getSpecialty());
            row.createCell(18).setCellValue(emp.getSchool());
            HSSFCell cell19 = row.createCell(19);
            cell19.setCellStyle(dateCellStyle);
            cell19.setCellValue(emp.getBeginDate());
            row.createCell(20).setCellValue(emp.getWorkState());
            row.createCell(21).setCellValue(emp.getEmail());
            row.createCell(22).setCellValue(emp.getContractTerm());
            HSSFCell cell23 = row.createCell(23);
            cell23.setCellStyle(dateCellStyle);
            cell23.setCellValue(emp.getBeginContract());
            HSSFCell cell24 = row.createCell(24);
            cell24.setCellStyle(dateCellStyle);
            cell24.setCellValue(emp.getEndContract());
            HSSFCell cell25 = row.createCell(25);
            cell25.setCellStyle(dateCellStyle);
            cell25.setCellValue(emp.getConversionTime());
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        HttpHeaders headers = new HttpHeaders();
        try {
            headers.setContentDispositionFormData("attachment", new String("员工表.xls".getBytes("UTF-8"), "ISO-8859-1"));
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            workbook.write(baos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<byte[]>(baos.toByteArray(), headers, HttpStatus.CREATED);
    }

    /**
     * Excel 解析成 员工数据集合
     *
     * @param file
     * @param allNations
     * @param allPoliticsstatus
     * @param allDepartments
     * @param allPositions
     * @param allJobLevels
     * @return
     */
    public static List<Employee> excel2Employee(MultipartFile file, List<Nation> allNations, List<Politicsstatus> allPoliticsstatus, List<Department> allDepartments, List<Position> allPositions, List<JobLevel> allJobLevels) {
        List<Employee> list = new ArrayList<>();
        Employee employee = null;
        try {
            //1. 创建一个 workbook 对象
            HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream());
            //2. 获取 workbook 中表单的数量
            int numberOfSheets = workbook.getNumberOfSheets();
            for (int i = 0; i < numberOfSheets; i++) {
                //3. 获取表单
                HSSFSheet sheet = workbook.getSheetAt(i);
                //4. 获取表单中的行数
                int physicalNumberOfRows = sheet.getPhysicalNumberOfRows();
                for (int j = 0; j < physicalNumberOfRows; j++) {
                    //5. 跳过标题行
                    if (j == 0) {
                        continue;//跳过标题行
                    }
                    //6. 获取行
                    HSSFRow row = sheet.getRow(j);
                    if (row == null) {
                        continue;//防止数据中间有空行
                    }
                    //7. 获取列数
                    int physicalNumberOfCells = row.getPhysicalNumberOfCells();
                    employee = new Employee();
                    for (int k = 0; k < physicalNumberOfCells; k++) {
                        HSSFCell cell = row.getCell(k);
                        switch (cell.getCellType()) {
                            case STRING:
                                String cellValue = cell.getStringCellValue();
                                switch (k) {
                                    case 1:
                                        employee.setName(cellValue);
                                        break;
                                    case 2:
                                        employee.setWorkID(cellValue);
                                        break;
                                    case 3:
                                        employee.setGender(cellValue);
                                        break;
                                    case 5:
                                        employee.setIdCard(cellValue);
                                        break;
                                    case 6:
                                        employee.setWedlock(cellValue);
                                        break;
                                    case 7:
                                        int nationIndex = allNations.indexOf(new Nation(cellValue));
                                        employee.setNationId(allNations.get(nationIndex).getId());
                                        break;
                                    case 8:
                                        employee.setNativePlace(cellValue);
                                        break;
                                    case 9:
                                        int politicstatusIndex = allPoliticsstatus.indexOf(new Politicsstatus(cellValue));
                                        employee.setPoliticId(allPoliticsstatus.get(politicstatusIndex).getId());
                                        break;
                                    case 10:
                                        employee.setPhone(cellValue);
                                        break;
                                    case 11:
                                        employee.setAddress(cellValue);
                                        break;
                                    case 12:
                                        int departmentIndex = allDepartments.indexOf(new Department(cellValue));
                                        employee.setDepartmentId(allDepartments.get(departmentIndex).getId());
                                        break;
                                    case 13:
                                        int jobLevelIndex = allJobLevels.indexOf(new JobLevel(cellValue));
                                        employee.setJobLevelId(allJobLevels.get(jobLevelIndex).getId());
                                        break;
                                    case 14:
                                        int positionIndex = allPositions.indexOf(new Position(cellValue));
                                        employee.setPosId(allPositions.get(positionIndex).getId());
                                        break;
                                    case 15:
                                        employee.setEngageForm(cellValue);
                                        break;
                                    case 16:
                                        employee.setTiptopDegree(cellValue);
                                        break;
                                    case 17:
                                        employee.setSpecialty(cellValue);
                                        break;
                                    case 18:
                                        employee.setSchool(cellValue);
                                        break;
                                    case 20:
                                        employee.setWorkState(cellValue);
                                        break;
                                    case 21:
                                        employee.setEmail(cellValue);
                                        break;
                                }
                                break;
                            default: {
                                switch (k) {
                                    case 4:
                                        employee.setBirthday(cell.getDateCellValue());
                                        break;
                                    case 19:
                                        employee.setBeginDate(cell.getDateCellValue());
                                        break;
                                    case 23:
                                        employee.setBeginContract(cell.getDateCellValue());
                                        break;
                                    case 24:
                                        employee.setEndContract(cell.getDateCellValue());
                                        break;
                                    case 22:
                                        employee.setContractTerm(cell.getNumericCellValue());
                                        break;
                                    case 25:
                                        employee.setConversionTime(cell.getDateCellValue());
                                        break;
                                }
                            }
                            break;
                        }
                    }
                    list.add(employee);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * @Author qixiaowei
     * @Description //TODO 读取excel
     * @Date 10:27 org.sang.common.poi
     * @Param [excelFile]
     * @return org.apache.poi.ss.usermodel.Workbook
     **/
    private static Workbook readExcel(MultipartFile excelFile) {
        String fileName = excelFile.getOriginalFilename();
        Workbook wb = null;
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }
        InputStream is = null;
        try {
            is = excelFile.getInputStream();
            //创建excel2003的文件文本抽取对象
            if (isExcel2003) {
                return wb = new HSSFWorkbook(is);
            } else {//创建excel2007的文件文本抽取对象
                return wb = new XSSFWorkbook(is);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wb;
    }

    /**
     * @Author qixiaowei
     * @Description //TODO [注意，解析的行，要保证填入的是字符串，数如果数字，估计解析不出来了]
     * @Date 7:20 org.sang.common.poi
     * @Param [file]
     * @return java.util.List<org.javaboy.vhr.model.SecurityPolicy>
     **/
    public static List<SecurityPolicy> getExcelDate(MultipartFile file) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        List<SecurityPolicy> emps = new ArrayList<>();
        // 读取Excel表格
        Workbook wb = readExcel(file);
        Sheet sheet = null; // 对应excel文档中的sheet
        Row row = null; // 对应一个sheet中的一行
        if (wb != null) {
            // 获取第一个sheet
            sheet = wb.getSheetAt(0);
            // 获取最大行数
            int physicalNumberOfRows = sheet.getPhysicalNumberOfRows();
            SecurityPolicy employee;
            //累计真实解析记录的条数（特殊条件-过滤）
            int cumulativeNumber=0;
            int topnumber=0;
            for (int j =8; j < physicalNumberOfRows; j++) {
                row = sheet.getRow(j);
                if (row == null) {
                    continue;
                }
                //解析到遇到空行，直接脱离循环，停止解析
                int num=getRowIsNull(row,4);
                if(num!=0){
                    System.out.println("【政务信息系统基本信息】表格，实际已解析到第 "+j+" 行");
                    cumulativeNumber+=1;
                    topnumber+=1;
                    System.out.println("【访问类业务申请】已经解析完毕，当前第 "+cumulativeNumber+" 行为空行，不再进行解析，需要跨越3行进行下一申请【发布类业务申请】解析");
                    if(topnumber>=2){
                        System.out.println("【发布类业务申请】已经解析完毕， "+cumulativeNumber+" 行为空行，不再进行解析");
                        break;
                    }else{
                        j=j+2;
                        System.out.println("****************==============整体表格，接下来从第 "+j+" 行，开始发布业务类型表格争析");
                        continue;
                    }
                }else{
                    cumulativeNumber+=1;
                    System.out.println(j+"****************当前第"+cumulativeNumber+"行非空行，进行解析");
                }
                System.out.println("****************######################################接下来从第 "+j+" 行，开始发布业务类型表格争析");
                int physicalNumberOfCells = row.getLastCellNum();
                employee = new SecurityPolicy();
                for (int k =0; k < physicalNumberOfCells; k++) {
                    Cell cell = row.getCell(k);
                    if (cell == null){ continue;}
                    /**
                     * POI Excel 单元格内容类型判断并取值
                     * 在读取每一个单元格的值的时候,通过getCellType方法获得当前单元格的类型,
                     **/
                    switch (cell.getCellTypeEnum()) {
                        case STRING :{
                            String cellValue = cell.getStringCellValue();  //直接按字符串进行取值
                            System.out.println("@@@@@@@@@@@@@@@@@@@@@第 "+k+" 列数据值为: "+cellValue);
                            if (cellValue == null) {
                                cellValue = "";
                            }
                            //注意：小于2代表解析的是访问类业务申请
                            if(topnumber<2){
                                switch (k) {
                                    case 1:
                                        employee.setPrivateIPAddress(cellValue);
                                        break;
                                    case 2:
                                        employee.setDestinationIPAddress(cellValue);
                                        break;
                                    case 3:
                                        employee.setProtocolType(cellValue);
                                        break;
                                    case 5:
                                        employee.setVisitBusinessDescription(cellValue);
                                        break;
                                    case 6:
                                        employee.setDurationVisit(cellValue);
                                        break;
                                    case 7:
                                        employee.setApplicationType(cellValue);
                                        System.out.println("############################################"+cellValue);
                                        break;
                                    case 8:
                                        employee.setRemarks(cellValue);
                                        break;
                                }
                            }else{
                                System.out.println("准备解析发布类");
                                switch (k) {
                                    case 1:
                                        employee.setPrivateIPAddress(cellValue);
                                        employee.setDestinationIPAddress(cellValue);//设置成一样,防止为空
                                        break;
                                    case 2:
                                        employee.setProtocolType(cellValue);
                                        break;
                                    case 4:
                                        employee.setVisitBusinessDescription(cellValue);
                                        break;
                                    case 5:
                                        employee.setApplicationType(cellValue);
                                        break;
                                    case 6:
                                        employee.setDurationVisit(cellValue);
                                        break;
                                    case 7:
                                        employee.setRemarks(cellValue);
                                        break;
                                }
                            }
                        }
                        break;
                        case NUMERIC:{
                            String cellValue=numOfImport(cell);//直接按数字进行取值
                            System.out.println("￥￥￥￥￥￥￥￥￥￥￥￥￥￥第 "+k+" 列数据值为: "+cellValue);
                            if (cellValue == null) {
                                cellValue = "";
                            }
                            //注意，上述会输出所有为数字的列，下则则仅取约定列赋值PO
                            //注意：小于2代表解析的是访问类业务申请
                            if(topnumber<2) {
                                switch (k) {
                                    case 0:
                                        employee.setBianhao(cellValue);
                                        break;
                                    case 4:
                                        employee.setDestinationPort(cellValue);
                                        break;
                                }
                            }else{
                                System.out.println("准备解析发布类");
                                switch (k) {
                                    case 0:
                                        employee.setBianhao(cellValue);
                                        break;
                                    case 3:
                                        employee.setDestinationPort(cellValue);
                                        break;
                                }
                            }
                            break;
                        }
                    }
                }
                if(topnumber>=2){
                    employee.setBusinessType("发布类业务申请");
                }else{
                    employee.setBusinessType("访问类业务申请");
                }
                emps.add(employee);
            }
        }
        return emps;
    }

    /**
     * 判断是否是空行
     * @param row  行对象
     * @param rowNum  需要判断的列数
     * @return
     */
    private static int getRowIsNull(Row row,int rowNum) {
        int num = 0;        //空单元格的数量
        for (int i = 0 ; i < rowNum ; i ++ ){
            Cell cell = row.getCell(i);
            if (null == cell){        //判断这个行是否为空
                num ++;
            }else if (cell.getCellType() == HSSFCell.CELL_TYPE_BLANK){  //空值
                num ++;
            }
        }
        return num;
    }


    /**
     * 处理导入小数点
     */
    public  static String  numOfImport(Cell cell) {
        String value = cell.toString();
        String[] str = value.split("\\.");
        if (str.length > 1) {
            String str1 = str[1];
            int m = Integer.parseInt(str1);
            if (m == 0) {
                return str[0];
            } else {
                return value;
            }
        }else{
            return value;
        }
    }

}
