#!/bin/sh

# 配置文件根目录，固定是hrserver
DOCKERHOME=/web/hnserver

# 镜像名称前缀、标签
BASE_IMAGE_NAME=registry.cn-beijing.aliyuncs.com/hrserver
BSEE_IMAGE_TAG=latest

# 各服务的镜像名称
UI_SERVICE=$BASE_IMAGE_NAME/hrserver-ui:$BSEE_IMAGE_TAG
MAIL_SERVICE=$BASE_IMAGE_NAME/mailserver:$BSEE_IMAGE_TAG
WEB_SERVICE=$BASE_IMAGE_NAME/vhr-web:$BSEE_IMAGE_TAG

case "$1" in

    # 删除容器
    removeAll)
        echo "* 正在删除容器..."
        time docker rm $(docker ps -aq) -f
        echo "* 删除容器成功..."
        ;;
    # 拉取镜像
    pull)
        echo "* 正在拉取后端镜像..."
        time docker pull $MAIL_SERVICE
        time docker pull $WEB_SERVICE
        echo "* 开始拉取前端镜像..."
        time docker pull $UI_SERVICE
        echo "* 拉取镜像成功..."
        ;;
    # 运行镜像
    run)
        echo "* 开始运行后端服务镜像..."
        time docker-compose -f -p $DOCKERHOME/docker-compose-services.yml up -d
        echo "* 等待10s..."
        echo "* 运行成功..."
        ;;
    # 拉取镜像并运行
    pullrun)
        echo "* 正在拉取后端镜像..."
        time docker pull $MAIL_SERVICE
        time docker pull $WEB_SERVICE
        echo "* 开始拉取前端镜像..."
        time docker pull $UI_SERVICE
        echo "* 拉取镜像成功..."

        echo "* 开始运行前端与后端服务镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-services.yml up -d
        echo "* 等待10s..."
        echo "* 运行成功..."
        ;;
    # 停止容器
    stop)
        echo "* 正在停止容器..."
        time docker-compose -f $DOCKERHOME/docker-compose-services.yml stop
        echo "* 停止容器成功..."
        ;;
    # 重启容器
    restart)
        echo "* 正在停止镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-services.yml restart
        ;;
    # 其它
    *)
        echo "* ..."
        ;;
esac
exit 0